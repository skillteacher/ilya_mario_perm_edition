using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class health : MonoBehaviour
{
    [SerializeField] private int maxLives;
    private Vector3 startPosition;
    private int lives;

    private void Start()
    {
        lives = maxLives;
        PlayerUi.ui.SetLives(lives);
        startPosition = transform.position;
    }
    public void TakeDamage(int damage)
    {
        lives -= damage;
        if (lives <= 0)
        {
            Death();
        }
        else
        {
            transform.position = startPosition;
        }
    }

    private void Death()
    {
        Game game = FindObjectOfType<Game>();
        game.GameOver(maxLives);
    }
}