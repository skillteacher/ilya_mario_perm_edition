using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerUi : MonoBehaviour
{
    public static PlayerUi ui;

    [SerializeField] private TMP_Text livesCount;
    [SerializeField] private TMP_Text coinsCount;

    private void Awake()
    {
        if(ui == null)
        {
            ui = this;
        }
        else
        {
            Destroy(this);
        }
    }
    public void SetLives(int amount)
    {
        if (amount < 0) amount = 0;
        livesCount.text = amount.ToString();
    }
    public void Setcoins(int amount)
    {
        coinsCount.text = amount.ToString();
    }

}
