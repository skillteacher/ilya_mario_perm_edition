using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    [SerializeField] private GameObject restartMenu;
    [SerializeField] private TMP_Text finalScore;
    [SerializeField] private string firstlevelName;
    private int coinCount = 0;
    private int currentMaxLives;
    

    private void Awake()
    {
        Game[] anotherGameScript = FindObjectsOfType<Game>();
        if(anotherGameScript.Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }

        restartMenu.SetActive(false);
    }

    public void AddCoins(int amount)
    {
        coinCount += amount;
        PlayerUi.ui.Setcoins(coinCount);
    }

    public void GameOver(int maxLives)
    {
        Time.timeScale = 0f;
        restartMenu.SetActive(true);
        finalScore.text = coinCount.ToString();
        currentMaxLives = maxLives;
    }

    public void RestartGame()
    {
        coinCount = 0;
        PlayerUi.ui.SetLives(currentMaxLives);
        SceneManager.LoadScene(firstlevelName);
        Time.timeScale = 1f;
        restartMenu.SetActive(false);
    }
}
